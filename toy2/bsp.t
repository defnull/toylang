var x := 5;    // static
var x  = 5;    // mutable

if 5 {a+c}

var add = {a, b, &c:
  if b == 0 {return a+c}
  a+b
};

add(5,6); // function call
map([1,2,3,4], {x: x*x});
[1..4].map({x:x*2}); // range(1,4,1).map({x:x*2})
[1..4..2]; // range(1,4,2)

dict([0..5].map({a: [0..a].map({&a,b: [a,b]}).filter({a,b: a==b})}));

(0..a-1).len;

// Getter are no-parameter functions
x = obj.attr;
x = obj.attr_get();

// Setter are single.parameter functions.
obj.attr = y;
obj.attr_set(y);
