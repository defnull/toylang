from modgrammar import *

grammar_whitespace_mode = 'optional'
Expr = REF("Expr")

class Comment(Grammar):
    grammar = L("//"), REST_OF_LINE

class Name(Grammar):
    grammar = WORD("a-zA-Z_","a-zA-Z_0-9")

class Int(Grammar):
    grammar = WORD("0-9")

class Float(Grammar):
    grammar = OPTIONAL(WORD("0-9")), '.', WORD("0-9")

class Number(Grammar):
    grammar = Int | Float




class ListLiteral(Grammar):
    grammar = L("["), LIST_OF(Expr), L("]")

class Return(Grammar):
    grammar = (L("return"), Expr)

class Decl(Grammar):
    grammar = ("var", LIST_OF(Name, OPTIONAL(L("=") | L(":="), Expr), separator=','))

class Assign(Grammar):
    grammar = (Name | (Expr, L("."), Name)), L("="), Expr

class Lambda(Grammar):
    grammar = L("{"), LIST_OF(Name | (L("&"), Name), min=0), L(":"), REF("Suite"), L("}")

class If(Grammar):
    grammar = L("if"), Expr, REF("Block")



'''
or_test: and_test ('or' and_test)*
and_test: not_test ('and' not_test)*
not_test: 'not' not_test | comparison
comparison: expr (comp_op expr)*
# <> isn't actually a valid comparison operator in Python. It's here for the
# sake of a __future__ import described in PEP 401 (which really works :-)
comp_op: '<'|'>'|'=='|'>='|'<='|'<>'|'!='|'in'|'not' 'in'|'is'|'is' 'not'
star_expr: '*' expr
expr: xor_expr ('|' xor_expr)*
xor_expr: and_expr ('^' and_expr)*
and_expr: shift_expr ('&' shift_expr)*
shift_expr: arith_expr (('<<'|'>>') arith_expr)*
arith_expr: term (('+'|'-') term)*
term: factor (('*'|'@'|'/'|'%'|'//') factor)*
factor: ('+'|'-'|'~') factor | power
power: atom_expr ['**' factor]
atom_expr: [AWAIT] atom trailer*
atom: ('(' [yield_expr|testlist_comp] ')' |
       '[' [testlist_comp] ']' |
       '{' [dictorsetmaker] '}' |
       NAME | NUMBER | STRING+ | '...' | 'None' | 'True' | 'False')
'''

class Literal(Grammar):
    grammar = (Name | Number | Lambda | ListLiteral)

class DotOp(Grammar):
    grammar = L("."), Name, OPTIONAL(REF("DotOp") | REF("CallOp"))

class CallOp(Grammar):
    grammar = L('('), LIST_OF(Expr, min=0), L(")"), OPTIONAL(DotOp | REF("CallOp"))

class TermExpr (Grammar):
    grammar = (Literal | (L('('), Expr, L(')'))), OPTIONAL(DotOp | CallOp)

class PowExpr (Grammar):
    grammar = TermExpr, ZERO_OR_MORE(OR("**"), TermExpr)

class MulDivExpr (Grammar):
    grammar = PowExpr, ZERO_OR_MORE(OR("*", "/"), PowExpr)

class AddSubExpr (Grammar):
    grammar = MulDivExpr, ZERO_OR_MORE(OR("+", "-"), MulDivExpr)

class RangeExpr(Grammar):
    grammar = AddSubExpr, OPTIONAL(L(".."), AddSubExpr, OPTIONAL(L(".."), AddSubExpr))

class BoolExpr (Grammar):
    grammar = RangeExpr, ZERO_OR_MORE(OR("==","!=",">","<",">=","<=","is","is not"), RangeExpr)

class OrExpr (Grammar):
    grammar = BoolExpr, ZERO_OR_MORE(OR("or", "||"), BoolExpr)

class AndExpr (Grammar):
    grammar = OrExpr, ZERO_OR_MORE(OR("and", "&&"), OrExpr)

class Expr(Grammar):
    grammar = AndExpr


class Stmt(Grammar):
    grammar = (Decl | Return | If | Expr | Assign)

class IStmt(Grammar):
    grammar = ((Stmt, L(";")) | If), OPTIONAL(Comment)

class TStmt(Grammar):
    grammar = Stmt, OPTIONAL(Comment)

class Block(Grammar):
    grammar = L("{"), REF("Suite"), L("}")

class Suite(Grammar):
    grammar = ZERO_OR_MORE(IStmt), OPTIONAL(TStmt)

parser = IStmt.parser(debug=True)
result = list(parser.parse_file('bsp.t'))

for stmt in result:
    print(stmt.elements)

print(*list(generate_ebnf(IStmt)))
