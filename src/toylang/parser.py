from .lexer import Lexer, Token, digit, alpha



class Syntax:
    def __init__(self):
        self.symbol_table = {}
        self.literal_rules = []
        self.comment_rules = []
        self.token_filter = []

    def parse(self, code, filename):
        return Parser(self, code, filename).parse()

    def filter(self):
        def bind(func):
            self.token_filter.append(func)
            return func
        return bind

    def literal(self, name):
        def bind(func):
            self.literal_rules.append((name, func))
            return func
        return bind

    def comment(self):
        def bind(func):
            self.comment_rules.append(func)
            return func
        return bind

    def symbol(self, symbol):
        def bind(klass):
            if not issubclass(klass, Node):
                raise TypeError('Must subclass Node')
            if not hasattr(klass, 'id'):
                klass.id = symbol
            self.symbol_table[symbol] = klass
            return klass
        return bind



class Node:
    lbp = rbp = 0
    
    def __init__(self, parser, token):
        self.parser = parser
        self.__token = token
        self.values = []
        self.parent = None
        
    def __eq__(self, other):
        if isinstance(other, Node):
            return self.id == other.id
        return self.id == other
        
    def __hash__(self):
        return hash(self.id)

    def append(self, value):
        value.parent = self
        self.values.append(value)
    
    @property
    def args(self):
        return self.__token.data

    @property
    def pos(self):
        return self.__token.filename, self.__token.lineno, self.__token.charno

    @property
    def indent(self):
        return self.__token.indent

    def nud(self):
        raise SyntaxError("Syntax error %r in %r." % (self.id, self.pos))

    def led(self, left):
        raise SyntaxError("Unknown operator %r in %r." % (self.id, self.pos))

    def skip(self):
        return False

    def __repr__(self):
        return self.id

    def tree(self):
        op = getattr(self, 'alias', self.id)
        args = (sub.tree() for sub in self.values)
        return op + "(" + ", ".join(args) + ")"



class Parser:

    def __init__(self, syntax, code, filename):
        self.syntax = syntax
        self.lexer = Lexer(syntax, code, filename)
        self.token_stream = iter(self.lexer)
        self.token_cache = []        
        self.par_stack = []
        self.current = None

    def _next_token(self):
        token = next(self.token_stream)

        if token == 'SYMBOL':
            node_class = self.syntax.symbol_table[token.data]
        else:
            node_class = self.syntax.symbol_table['<%s>' % token.id]

        node = node_class(self, token)
        node.last = self.current
        self.current = node
        return node

    def pop(self):
        while True:
            if self.token_cache:
                token = self.token_cache.pop(0)
            else:
                token = self._next_token()
            if not token.skip():
                return token

    def peek(self, offset=0):
        for token in self.token_cache:
            if not token.skip():
                offset -=1
                if offset < 0:
                    return token

        peeked = []
        while True:
            token = self._next_token()
            peeked.append(token)
            if not token.skip():
                offset -= 1
                if offset < 0:
                    self.token_cache.extend(peeked)
                    return token

    def check(self, *ids):
        found = self.peek()
        if found in ids:
            return found

    def expect(self, *ids):
        found = self.peek()
        if found not in ids:
            raise SyntaxError("Expected %r, found %r" % (ids, found))
        return self.pop()
        
    def eat(self, *ids):
        popped = []
        while self.peek() in ids:
            popped.append(self.pop())
        return popped

    def parse(self):
        while True:
            self.eat('<NL>')
            if self.peek() == '<EOF>':
                return
            stmt = self.statement('<NL>', ';')
            print('stmt', stmt.tree())

    def statement(self, *ids):
        e = self.expression()
        self.expect(*ids)
        return e

    def expression(self, bp=0, left=None):
        while not left:
            t = self.pop()
            left = t.nud()
        while True:
            next = self.peek()
            if bp >= next.lbp:
                break
            t = self.pop()
            left = t.led(left) or left
        return left


