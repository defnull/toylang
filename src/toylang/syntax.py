from .parser import Syntax, Node
from .lexer import digit, alpha

toylang = Syntax()

@toylang.comment()
def l_comment(lexer):
    if lexer.peek() not in '#':
        return False
    lexer.skip()
    comment = lexer.readwhile(lambda x: x not in '\n')
    return comment


@toylang.literal('STRING')
def l_string(lexer):
    if lexer.peek() not in "\"'":
        return False

    quote = lexer.read()
    buf = []
    while True:
        n = lexer.read()
        if n == "\\":
            buf.append(read_escaped_string_sequnece(lexer, quote))
        elif n == quote:
            return quote, ''.join(buf)
        elif ord(n) < 32 or ord(n) == 127:
            lexer.error("Restricted character in String literal.")
        else:
            buf.append(n)


def read_escaped_string_sequnece(lexer, quote):
    n = lexer.read()
    if n == 'r':     return '\r'
    elif n == 'n':   return '\n'
    elif n == 't':   return '\t'
    elif n == '0':   return '\0'
    elif n == '\\':  return '\\'
    elif n == quote: return quote
    elif n == 'x':
        return chr(int(lexer.read(2), 16))
    else:
        lexer.error('Unknown escape sequence in String literal.')


@toylang.literal('NUMBER')
def l_number(lexer):
    '''
        NUMBER = <num> | <num><e>
        num = <digit> | <digit> '.' <digit> | '.' <digit>
        e = 'e' <digit> | 'e-' <digit>
    '''
    a, b, c = None, None, None #A.BeC#

    if lexer.peek() in digit:
        a = int(lexer.readwhile(lambda x: x in digit))

    if lexer.peek() == '.' and lexer.peek(2)[1] in digit:
        lexer.skip()
        b = lexer.readwhile(lambda x: x in digit)
        b = int(b) if b else 0

    if a is b is None:
        return False

    if lexer.peek() == 'e':
        p3 = lexer.peek(3)
        if len(p3) >= 2 and p3[1] in digit:
            lexer.skip() # the 'e'
            c = int(lexer.readwhile(lambda x: x in digit))
        elif len(p3) == 3 and p3[1] == '-' and p3[2] in digit:
            lexer.skip(2) # the 'e-'
            c = -int(lexer.readwhile(lambda x: x in digit))

    return a, b, c




class LiteralNode(Node):
    def nud(self):
        return self


@toylang.symbol('<LITERAL:NUMBER>')
class Number(LiteralNode):
    def tree(self):
        a,b,c = self.args
        a = a or 0

        if b is None:
            return '%de%s' % (a,c) if c else '%d' % a
        else:
            return '%d.%de%s' % (a,b,c) if c else '%d.%d' % (a,b)


@toylang.symbol('<LITERAL:STRING>')
class Number(LiteralNode):
    def tree(self):
        quote, content = self.args
        return repr(content)


@toylang.symbol('<WORD>')
class WordNode(Node):

    def nud(self):
        return self
    
    def tree(self):
        return self.args


@toylang.symbol('<NL>')
class NewlineNode(Node):
    pass


@toylang.symbol('<WS>')
class WhitespaceNode(Node):
    def skip(self):
        return True


@toylang.symbol('<EOF>')
class EofNode(Node):
    pass




class Prefix(Node):
    def nud(self):
        self.append(self.parser.expression(self.rbp))
        return self

class Infix(Node):
    def led(self, left):
        self.append(left)
        self.append(self.parser.expression(self.lbp))
        return self

class Suffix(Node):
    def led(self, left):
        self.append(left)
        return self

class Infix_r(Node):
    def led(self, left):
        self.append(left)
        self.append(self.parser.expression(self.lbp-1))
        return self







def op(symbol, alias=None, bases=(Node,), **kargs):
    _alias = alias
    @toylang.symbol(symbol)
    class Symbol(*bases):
        alias = _alias or symbol
        lbp = kargs.get('lbp', 0)
        rbp = kargs.get('rbp', 0)

op("or", "or", (Infix_r,), lbp=30)
op("and", "and", (Infix_r,), lbp=40)
op("not", "not", (Prefix, Infix), lbp=60, rbp=50)
op("in", "in", (Infix,), lbp=60)
op("is", "is", (Infix,), lbp=60)
op("<", "lt", (Infix,), lbp=60)
op("<=", "lte", (Infix,), lbp=60)
op(">", "gt", (Infix,), lbp=60)
op(">=", "gte", (Infix,), lbp=60)
op("!=", "ne", (Infix,), lbp=60)
op("==", "eq", (Infix,), lbp=60)

op("|", "bor", (Infix,), lbp=70)
op("^", "bnot", (Infix,), lbp=80)
op("&", "band", (Infix,), lbp=90)
op("<<", "bls", (Infix,), lbp=100)
op(">>", "brs", (Infix,), lbp=100)

op("+", "add", (Infix, Prefix), lbp=110, rbp=130)
op("-", "sub", (Infix, Prefix), lbp=110, rbp=130)
op("*", "mul", (Infix,), lbp=120)
op("/", "div", (Infix,), lbp=120)
op("//", "fdv", (Infix,), lbp=120)
op("%", "mod", (Infix,), lbp=120)

op("=", "set", (Infix,), lbp=10)
op("**", "pow", (Infix_r,), lbp=140)

op(".", "dot", (Infix,), lbp=150)

op(",")
op(")")

@toylang.symbol('(')
class CallNode(Infix):

    lbp = 145
    alias = 'call'

    def read_until(self, end_par=')'):
        self.parser.par_stack.append(self)
        self.parser.eat('<NL>')
        if self.parser.peek() != ')':
            while True:
                self.parser.eat('<NL>')
                self.append(self.parser.expression())
                self.parser.eat('<NL>')
                if self.parser.eat(','):
                    self.parser.eat('<NL>')
                if self.parser.peek() == ')':
                    self.parser.par_stack.pop()
                    self.closing = self.parser.pop()
                    return self

    def led(self, left):
        self.id = self.alias = 'call'
        self.append(left)
        self.read_until(')')
        return self

    def nud(self):
        self.read_until(')')

        if len(self.values) == 0:
            self.id = self.alias = 'nop'      
        elif len(self.values) == 1:
            return self.values[0]
        else:
            self.id = self.alias = 'tuple'

        return self


parens = dict('() [] {}'.split())
class ParNode(Node):

    def nud(self, par=None):
        par = par or self.id
        end_par = parens[par]
        self.parser.par_stack.append(self)
        self.parser.eat('<NL>')
        if self.parser.peek() != end_par:
            while True:
                self.parser.eat('<NL>')
                self.append(self.parser.expression())
                self.parser.eat('<NL>')
                if self.parser.peek() == ',':
                    self.parser.pop()
                    self.parser.eat('<NL>')
                if self.parser.peek() == end_par:
                    self.parser.par_stack.pop()
                    self.closing = self.parser.pop()
                    return self

        return self

    def led(self, left):
        par = self.id
        if left == '<WORD>':
            self.id = 'call'
            self.append(left)
            self.nud(par)
        return self

    def tree(self):
        a, b = self.id, self.closing.id
        if len(self.values) == 0:
            return a+b
        elif len(self.values) == 1:
            return self.values[0].tree()
        else:
            return a + ', '.join(v.tree() for v in self.values) + b

#for a, b in parens.items():
#    op(a, None, (ParNode,), lbp=110)
#    op(b, None, (Node,))


op(':')
op(';')


@toylang.symbol('return')
class ReturnNode(Prefix):
    def nud(self):
        self.append(self.parser.expression())
        return self

@toylang.symbol('block')
class BlockNode(Node):
    def parse_block(self):
        block_start = self.parser.expect(':')
        if self.parser.peek() == '<NL>':
            self.parser.eat('<NL>')
            first = self.parser.peek()
            if block_start.indent >= first.indent:
                raise SyntaxError('Indentation error.')
            while True:
                self.append(self.parser.expression())
                next = self.parser.peek()
                if next in ('<EOF>',')','}',']'):
                    return # definately end of block
                if next == ';':
                    self.parser.pop()
                    continue
                while next == '<NL>':
                    # might be end of block, but only if next line
                    # is less indented.
                    nextnext = self.parser.peek(1)
                    if nextnext == '<NL>':
                        # This line was empty, we can safely drop it
                        next = self.parser.pop()
                    elif nextnext.indent < first.indent:
                        # Block ends here! But we do not consume the last
                        # newline because we need it later to end the
                        # statement.
                        return
                    elif nextnext.indent == first.indent:
                        self.parser.pop()
                        break
        else:
            self.append(self.parser.expression())

    def nud(self):
        self.parse_block()
        return self



if __name__ == '__main__':
    import sys
    code = sys.stdin.read()
    toylang.parse(code, 'fo.py')

