from collections import namedtuple

alpha = 'abcdefghijklmnopqrstuvwxyz'
alpha += alpha.upper()
digit = '0123456789'


class Token(namedtuple('Token', 'id data indent lineno charno filename')):
    
    def __eq__(self, other):
        if isinstance(other, Token):
            return self.id == other.id
        return self.id == other
    
    def __repr__(self):
        return '%s<token %s%r in %r %d+%d>' % (' '*self.indent, self.id,
                                               self.data, self.filename,
                                               self.lineno, self.charno)



class Lexer:
    def __init__(self, syntax, code, filename):
        self.syntax = syntax
        self.code = code
        self.filename = filename
        self.indent = 0
        self.lineno = 1
        self.charno = 0
        self.index = 0
        self.eof_sent = False
        self.found_non_whitespace = False

    def read(self, n=1):
        ret = self.code[self.index:self.index+n]

        if len(ret) < n:
            self.error('Unexpected end of file')

        self.index += len(ret)
        for c in ret:
            if ret == '\n':
                self.lineno += 1
                self.charno += 0
                self.indent = 0
                self.found_non_whitespace = False
            elif ret in ' \t':
                if not self.found_non_whitespace:
                    self.indent += 1
            else:
                self.found_non_whitespace = True

        return ret

    def eof(self):
        return self.index >= len(self.code)

    def peek(self, n=1, offset=0):
        ret = self.code[self.index+offset:self.index+offset+n]
        if len(ret) < n:
            self.error('Unexpected end of file')
        return ret

    def skip(self, n=1):
        self.read(n)

    def find(self, search):
        return max(-1, self.code.find(search, self.index) - self.index)

    def readwhile(self, cond, n=1, ahead=0):
        ''' Read chunks of size n as long as cond(read(n+ahead)) returns true'''
        buf = ''
        while not self.eof() and cond(self.peek(n+ahead)):
            buf += self.read(n)
        return buf

    def error(self, msg):
        indent, lineno, charno, filename = self.last_pos
        raise SyntaxError('%s (LINE %d CHAR %d)' % (msg, lineno, charno))

    def _get_next_token(self):
        self.last_pos = self.indent, self.lineno, self.charno, self.filename

        if not self.eof():
            for match in (self.t_WHITESPACE,
                          self.t_COMMENT,
                          self.t_SYMBOL,
                          self.t_LITERAL,
                          self.t_WORD):
                ret = match()
                if not ret:
                    continue
                return Token(ret[0], ret[1], *self.last_pos)
            else:
                return Token('OTHER', (self.read(),), *self.last_pos)
    
    def __iter__(self):
        stream = iter(self._get_next_token, None)
        for f in self.syntax.token_filter:
            stream = f(stream)
        yield from stream
        yield Token('EOF', (), *self.last_pos)

    def lex(self):
        return list(self)
        
    def iterlex(self):
        return iter(self)

    def t_WHITESPACE(self):
        if self.peek() in ' \t':
            return ('WS', self.read())
        if self.peek() in '\n':
            return ('NL', self.read())
        return False

    def t_COMMENT(self):
        for f in self.syntax.comment_rules:
            literal = f(self)
            if literal:
                return ('COMMENT', literal)
        return False

    def t_SYMBOL(self):
        for symbol in sorted(self.syntax.symbol_table, key=len, reverse=True):
            if self.find(symbol) == 0:
                self.skip(len(symbol))
                return ('SYMBOL', symbol)
        return False

    def t_LITERAL(self):
        for name, match in self.syntax.literal_rules:
            value = match(self)                    
            if value:
                return ('LITERAL:{}'.format(name), value)
        return False

    def t_WORD(self):
        c = self.peek()
        if c not in alpha+'_':
            return False
        word = self.read()
        word += self.readwhile(lambda x: x in alpha+digit+'_')
        return ('WORD', word)

